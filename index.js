// console.log("Hello World")

// An array is simply a list of data. Used to store multiple related values in a single variable.

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

// [SECTION] [] - array literals

let myTasks = ["drink html",
	"eat javascript", 
	"inhale css", 
	"bake sass"]


	let city1 = "Tokyo";
	let city2 = "Manila";
	let city3 = "Jakarta";

	let cities =[city1, city2, city3];
	console.log(myTasks);
	console.log(cities);

// Length Property .length

console.log(myTasks.length);
console.log(cities.length);
 
let blankArr = [];
console.log(blankArr.length);

// Length property can be used with strings

let fullName = "Jamie Noble";
console.log(fullName.length);

// Length property can also set the total number of items in an array.
myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

// To delete a specific item we can empoy array methods in s22

// Another example of decrementation

cities.length--;
console.log(cities);

// We can't do this on strings

// Incrementation
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

/*
    - Accessing array elements is one of the more common tasks that we do with an array
    - This can be done through the use of array indexes
    - Each element in an array is associated with it's own index/number
    - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
    - The reason an array starts with 0 is due to how the language is designed- Array indexes actually refer to an address/location in the device's memory and how the information is stored
    - Example array location in memory
        Array address: 0x7ffe9472bad0
        Array[0] = 0x7ffe9472bad0
        Array[1] = 0x7ffe9472bad4
        Array[2] = 0x7ffe9472bad8
    - In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself- Syntax
        arrayName[index];
*/
let grades = ["60", "70", "80", "90"];
let computerBrands = ["Asus", "Lenovo", "Apple"];
console.log(grades[0]);
console.log(computerBrands[3]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[2]);

let currentLaker = lakersLegends[2];
console.log(currentLaker);

console.log("Array before re-assignment");
console.log(lakersLegends);

lakersLegends[2]="Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length -1;
console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[lastElementIndex -1]);

let newArray = [];
console.log(newArray);
newArray[0] = "Cloud Strife";
console.log(newArray);

console.log(newArray[1]);
newArray[1] = "Tifa Lockhart";
console.log(newArray);

newArray[newArray.length] = "Barrett Wallace";
console.log(newArray);

let numbers = [5, 12, 30, 46, 40];

for(let index = 0; index < numbers.length; index++){
	if(numbers[index] % 5 === 0){
		console.log(numbers[index] + " is divisible by 5");
	}else{
		console.log(numbers[index] + " is not divisible by 5")
	}
}

// [SECTION] Multi-dimensional arrays
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
console.table(chessBoard);
// [1] -> Column	[4] -> Row
console.log(chessBoard[1][4]);
console.log("Pawn moves to: " + chessBoard[1][5])